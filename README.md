# AdgroupExamGabAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 11.0.4.

## NOTE: Make sure to install the node and npm to your machine.

## Installation
1. Run `npm install -g @angular/cli` to install angular to your machine.
2. Clone the repository to your local and checkout to `develop` branch.
2. Go to the root directory of the project and run `npm install` to install the node modules based on the `package.json`
3. Run `ng serve`,  open the browser and go to `http://localhost:4200`

## How it works
1. Go to registration page.
2. Register your account.
3. Login using your account.
4. Add new IP Address and label.

Saving and updating activities are being logged into the database. You can go to `Activity Logs` menu at the header to see.