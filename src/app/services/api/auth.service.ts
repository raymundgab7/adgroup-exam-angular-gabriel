import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Router} from "@angular/router";
import {IUser} from "../../models/IUser";
import {Observable} from "rxjs";
import {tap} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private http: HttpClient, private router: Router) { }

  register(data: IUser): Observable<any> {
    return this.http.post<any>('api/register', data );
  }

  signIn(data: IUser): Observable<any> {
    return this.http.post<any>('api/login', data );
  }

  signOut(): Observable<any> {
    return this.http.get<any>('/api/logout').pipe(
        tap(req => {
          this.destroyToken();
        })
    );
  }

  setToken(req: any): void {
    if (req) {
      localStorage.removeItem('AccessToken');
      localStorage.setItem('AccessToken', req.access_token);
    }
  }

  destroyToken() {
    localStorage.removeItem('AccessToken');
  }

  redirectWithoutToken() {
    this.destroyToken();
    this.router.navigate(['login']);
  }
}
