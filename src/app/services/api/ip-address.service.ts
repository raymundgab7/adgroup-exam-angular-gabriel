import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";
import {IPAddress} from "../../models/IPAddress";

@Injectable({
  providedIn: 'root'
})
export class IpAddressService {

  constructor(private http: HttpClient, private router: Router) { }

  getIpAddressById(id: string | null): Observable<any> {
    return this.http.get<any>(`api/ip-address/${id}` );
  }

  getAllIpAddress(): Observable<any> {
    return this.http.get<any>('api/ip-address' );
  }

  createIpAddress(data: IPAddress): Observable<any> {
    return this.http.post<any>('api/ip-address', data );
  }

  updateIpAddress(data: IPAddress, id: string | null | undefined): Observable<any> {
    return this.http.put<any>(`api/ip-address/${id}`, data );
  }

}
