import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Router} from "@angular/router";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ActivityLogsService {

  constructor(private http: HttpClient, private router: Router) { }

  getAllActivityLogs(): Observable<any> {
    return this.http.get<any>(`api/activity`);
  }

  getActivityLogsById(id: string): Observable<any> {
    return this.http.get<any>(`api/activity/${id}`);
  }
}
