import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})

@Injectable()
export class PermissionService {
  public isUserLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  constructor() { }

  isAuthenticated(): boolean {
    return !!localStorage.getItem('AccessToken');
  }

  getToken(): string {
    return <string>localStorage.getItem('AccessToken');
  }

}
