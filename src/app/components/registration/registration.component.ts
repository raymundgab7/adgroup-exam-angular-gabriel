import {Component, OnDestroy, OnInit} from '@angular/core';
import {IUser} from "../../models/IUser";
import {Subject} from "rxjs";
import {Title} from "@angular/platform-browser";
import {AuthErrorService} from "../../services/lang/en-text/auth-error.services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {AuthService} from "../../services/api/auth.service";
import {PermissionService} from "../../services/shared/permission.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {compareValidator} from "../../directive/compare.directive";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit, OnDestroy {

  user: IUser | undefined;
  public destroyed = new Subject<any>();
  isAuthenticated = false;
  constructor(
      private titleService: Title,
      private snackBar: MatSnackBar,
      private auth: AuthService,
      private permission: PermissionService,
      private router: Router
  ) {
    this.permission.isUserLoggedIn.subscribe( value => {
      this.isAuthenticated = value;
    });
  }

  registrationForm = new FormGroup({
    name: new FormControl('', Validators.compose([
      Validators.required
    ])),
    email: new FormControl('', Validators.compose([
      Validators.required,
      Validators.email
    ])),
    password: new FormControl('', Validators.compose([
      Validators.minLength(5),
      Validators.required
    ])),
    password_confirmation: new FormControl('', Validators.compose([
      Validators.required,
      compareValidator('password')
    ])),
  });

  ngOnInit(): void {
    this.titleService.setTitle('Login');
  }
  get f() { return this.registrationForm.controls; }

  onSubmit() {
    this.user = new IUser();
    this.user.name = this.f.name.value.trim();
    this.user.email = this.f.email.value.trim();
    this.user.password = this.f.password.value.trim();
    this.user.password_confirmation = this.f.password_confirmation.value.trim();;

    this.auth.register(this.user).subscribe(
        req => {
          this.router.navigate(['/login']);
        },
        error => {
          this.snackBar.open('Error : ' + error.error.error, '', {
            duration: 3000
          });
        }
    );
  }
  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

}
