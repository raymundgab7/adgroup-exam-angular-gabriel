import {Component, OnDestroy, OnInit} from '@angular/core';
import { IUser } from "../../models/IUser";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AuthService} from "../../services/api/auth.service";
import {Router} from "@angular/router";
import { Title } from '@angular/platform-browser';
import { MatSnackBar } from '@angular/material/snack-bar';
import {AuthErrorService} from "../../services/lang/en-text/auth-error.services";
import {Subject} from "rxjs";
import {PermissionService} from "../../services/shared/permission.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})

export class LoginComponent implements OnInit, OnDestroy {
  user: IUser | undefined;
  public destroyed = new Subject<any>();
  isAuthenticated = false;
  constructor(
    private titleService: Title,
    private snackBar: MatSnackBar,
    private auth: AuthService,
    private permission: PermissionService,
    private router: Router
  ) {
    this.permission.isUserLoggedIn.subscribe( value => {
      this.isAuthenticated = value;
    });
  }

  loginForm = new FormGroup({
    email: new FormControl('', Validators.compose([
      Validators.required,
      Validators.email
    ])),
    password: new FormControl('', Validators.compose([
      Validators.minLength(5),
      Validators.required
    ])),
  });

  ngOnInit(): void {
    this.titleService.setTitle('Login');
  }
  get f() { return this.loginForm.controls; }

  onSubmit() {
    this.user = new IUser();
    this.user.email = this.f.email.value.trim();
    this.user.password = this.f.password.value.trim();

    this.auth.signIn(this.user).subscribe(
      req => {
        this.auth.setToken(req);
        this.permission.isUserLoggedIn.next(true);
        this.router.navigate(['/ip-address']);
      },
      error => {
        console.log(error.error.error)
        if(error.error.error === undefined) {
          this.snackBar.open('Error : ' + error.error.message, '', {
            duration: 3000
          });
        } else {
          this.snackBar.open('Error : ' + error.error.error, '', {
            duration: 3000
          });
        }

      }
    );
  }
  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }
}
