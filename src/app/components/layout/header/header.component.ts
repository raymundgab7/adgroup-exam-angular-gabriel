import {Component, OnDestroy, OnInit} from '@angular/core';
import { PermissionService } from '../../../services/shared/permission.service';
import {ActivatedRoute, Router} from "@angular/router";
import { AuthService } from "../../../services/api/auth.service";
import {Subject} from "rxjs";
import {tap} from "rxjs/operators";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public destroyed = new Subject<any>();
  isAuthenticated = false;
  constructor(
      private permission: PermissionService,
      private router: Router,
      private auth: AuthService,
      private activeRoute: ActivatedRoute
  ) {
    this.permission.isUserLoggedIn.subscribe( value => {
      this.isAuthenticated = value;
    });
  }

  ngOnInit(): void {
    this.isAuthenticated = this.permission.isAuthenticated()
  }


  signOut() {
    this.auth.signOut().subscribe(
      req => {
        this.permission.isUserLoggedIn.next(false);
        this.router.navigate(['login']);
      })
  }

  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }

}
