import {Component, OnDestroy, OnInit} from '@angular/core';
import { IpAddressService } from "../../../services/api/ip-address.service";
import {Observable, Subject, Subscription} from "rxjs";
import {Router} from "@angular/router";

@Component({
  selector: 'app-ip-address-list',
  templateUrl: './ip-address-list.component.html',
  styleUrls: ['./ip-address-list.component.css']
})
export class IpAddressListComponent implements OnInit, OnDestroy {
  public destroyed = new Subject<any>();
  ipAddresses: {} | undefined;
  constructor(
      private ipAddressService: IpAddressService,
      private router: Router
  ) { }


  ngOnInit() {
    this.getAllIpAddress();
  }
  private getAllIpAddress() {
    this.ipAddressService.getAllIpAddress().subscribe(res => {
      this.ipAddresses = res.ipAddress;
    });
  }
  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }
}
