import {Component, OnDestroy, OnInit} from '@angular/core';
import {IPAddress} from "../../../models/IPAddress";
import {Title} from "@angular/platform-browser";
import {AuthErrorService} from "../../../services/lang/en-text/auth-error.services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {IpAddressService} from "../../../services/api/ip-address.service";
import {ActivatedRoute, Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {Subject} from "rxjs";

@Component({
  selector: 'app-ip-address-edit',
  templateUrl: './ip-address-edit.component.html',
  styleUrls: ['./ip-address-edit.component.css']
})
export class IpAddressEditComponent implements OnInit, OnDestroy {
  public destroyed = new Subject<any>();
  ipAddressDetails: any ;
  id: string | null | undefined;
  public progressBar: boolean = false;

  constructor(
      private titleService: Title,
      public sgValidation: AuthErrorService,
      private snackBar: MatSnackBar,
      private ipAddressService: IpAddressService,
      private router: Router,
      private route: ActivatedRoute
  ) { }

  ipAddressEditForm = new FormGroup({
    id: new FormControl('', Validators.compose([
      Validators.required
    ])),
    ip_address: new FormControl('', Validators.compose([
      Validators.required
    ])),
    label: new FormControl('', Validators.compose([
      Validators.required
    ])),
  });

  ngOnInit(): void {
    this.titleService.setTitle('Edit IP Address');
    this.id = this.route.snapshot.paramMap.get('id');
    this.getIpAddress(this.id)
  }

  get f() { return this.ipAddressEditForm.controls; }

  private getIpAddress(id: string | null) {
    this.ipAddressService.getIpAddressById(id).subscribe(res => {
      this.ipAddressEditForm.setValue(({
        id: res.ipAddress.id,
        ip_address: res.ipAddress.ip_address,
        label: res.ipAddress.label
      }));
    });

  }

  onSubmit() {

    this.progressBar = true;

    this.ipAddressDetails = new IPAddress();
    this.ipAddressDetails.ip_address = this.f.ip_address.value.trim();
    this.ipAddressDetails.label = this.f.label.value.trim();

    this.ipAddressService.updateIpAddress(this.ipAddressDetails, this.id).subscribe(
        req => {

          this.snackBar.open('Success : Successfully created!', '', {
            duration: 3000
          });


          this.router.navigate(['/ip-address']);
        },
        error => {
          this.progressBar = false;
          this.snackBar.open('Error : ' + error.error.error, '', {
            duration: 3000
          });

        }
    );
  }
  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }
}
