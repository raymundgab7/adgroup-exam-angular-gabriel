import {Component, OnDestroy, OnInit} from '@angular/core';
import {Title} from "@angular/platform-browser";
import {AuthErrorService} from "../../../services/lang/en-text/auth-error.services";
import {MatSnackBar} from "@angular/material/snack-bar";
import {IpAddressService } from "../../../services/api/ip-address.service";
import {Router} from "@angular/router";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {IPAddress} from "../../../models/IPAddress";
import {Subject} from "rxjs";

@Component({
  selector: 'app-ip-address-create',
  templateUrl: './ip-address-create.component.html',
  styleUrls: ['./ip-address-create.component.css']
})
export class IpAddressCreateComponent implements OnInit, OnDestroy {
  ipAddress: IPAddress | undefined ;
  public destroyed = new Subject<any>();
  public progressBar: boolean = false;

  constructor(
      private titleService: Title,
      public sgValidation: AuthErrorService,
      private snackBar: MatSnackBar,
      private ipAddressService: IpAddressService,
      private router: Router
  ) { }

  ipAddressForm = new FormGroup({
    ip_address: new FormControl('', Validators.compose([
      Validators.required
    ])),
    label: new FormControl('', Validators.compose([
      Validators.required
    ])),
  });

  ngOnInit(): void {
    this.titleService.setTitle('IP Address');
  }
  get f() { return this.ipAddressForm.controls; }
  onSubmit() {

    this.progressBar = true;

    this.ipAddress = new IPAddress();
    this.ipAddress.ip_address = this.f.ip_address.value.trim();
    this.ipAddress.label = this.f.label.value.trim();

    this.ipAddressService.createIpAddress(this.ipAddress).subscribe(
        req => {

          this.snackBar.open('Success : Successfully created!', '', {
            duration: 3000
          });


          this.router.navigate(['/ip-address']);
        },
        error => {
          this.progressBar = false;
          this.snackBar.open('Error : ' + error.error.error, '', {
            duration: 3000
          });

        }
    );
  }
  ngOnDestroy(): void {
    this.destroyed.next();
    this.destroyed.complete();
  }
}
