import { Component, OnInit } from '@angular/core';
import {ActivityLogsService} from "../../services/api/activity-logs.service";

@Component({
  selector: 'app-activity-logs',
  templateUrl: './activity-logs.component.html',
  styleUrls: ['./activity-logs.component.css']
})
export class ActivityLogsComponent implements OnInit {
  activityLogs: any;
  constructor(
      private activityLogsService: ActivityLogsService
  ) { }

  ngOnInit() {
    this.getAllActivityLogs();
  }
  private getAllActivityLogs() {
    this.activityLogsService.getAllActivityLogs().subscribe(res => {
      this.activityLogs = res.activityLogger;
    });
  }



}
