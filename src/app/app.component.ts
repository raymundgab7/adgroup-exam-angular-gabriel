import {Component, OnInit} from '@angular/core';
import {PermissionService} from "./services/shared/permission.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit{
  title = 'adgroup-exam-gab-angular';

  isAuthenticated = false;
  constructor(
      private permission: PermissionService
  ) { }

  ngOnInit(): void {
    this.isAuthenticated = this.permission.isAuthenticated()
  }

}

