import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { IpAddressListComponent } from "./components/ip-address/ip-address-list/ip-address-list.component";
import { AuthGuardService } from "./services/guard/auth-guard.services";
import { NonAuthGuard } from './services/guard/non-auth.guard';
import { IpAddressCreateComponent } from "./components/ip-address/ip-address-create/ip-address-create.component";
import { IpAddressEditComponent } from "./components/ip-address/ip-address-edit/ip-address-edit.component";
import {ActivityLogsComponent} from "./components/activity-logs/activity-logs.component";
import {RegistrationComponent} from "./components/registration/registration.component";

const routes: Routes = [
  {
    path: "login", component: LoginComponent, canActivate: [NonAuthGuard],runGuardsAndResolvers: 'always'}, {
    path: "register", component: RegistrationComponent, canActivate: [NonAuthGuard],runGuardsAndResolvers: 'always' }, {
    path: 'ip-address/create', component: IpAddressCreateComponent, canActivate: [AuthGuardService] }, {
    path: 'ip-address', component: IpAddressListComponent, canActivate: [AuthGuardService] }, {
    path: 'ip-address/edit/:id', component: IpAddressEditComponent, canActivate: [AuthGuardService] }, {
    path: 'activity-logs', component: ActivityLogsComponent, canActivate: [AuthGuardService] }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {onSameUrlNavigation: `reload`})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
