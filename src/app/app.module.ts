import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { HeaderComponent } from './components/layout/header/header.component';
import { FooterComponent } from './components/layout/footer/footer.component';
import { ControlContainer, FormsModule, ReactiveFormsModule} from "@angular/forms";
import { environment } from "../environments/environment";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import { AuthService } from "./services/api/auth.service";
import { AuthInterceptor } from "./services/http/auth.interceptor";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { IpAddressCreateComponent } from './components/ip-address/ip-address-create/ip-address-create.component';
import { IpAddressListComponent } from './components/ip-address/ip-address-list/ip-address-list.component';
import { IpAddressService } from "./services/api/ip-address.service";
import { IpAddressEditComponent } from './components/ip-address/ip-address-edit/ip-address-edit.component';
import { ActivityLogsComponent } from './components/activity-logs/activity-logs.component';
import {PermissionService} from "./services/shared/permission.service";
import { RegistrationComponent } from './components/registration/registration.component';
import { CompareDirective } from './directive/compare.directive';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HeaderComponent,
    FooterComponent,
    IpAddressCreateComponent,
    IpAddressListComponent,
    IpAddressEditComponent,
    ActivityLogsComponent,
    RegistrationComponent,
    CompareDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatSnackBarModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    BrowserAnimationsModule
  ],
  providers: [
    AuthService,
    IpAddressService,
    {
      provide: "BASE_API_URL",
      useValue: environment.apiUrl
    },
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true },
    PermissionService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
